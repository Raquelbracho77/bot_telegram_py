def buscar(update, context):
    bot = context.bot
    chat_id = update.effective_chat.id
    user = update.message.from_user
    text = f'Hola {user.username}\n\n'
    argumentos = context.args

    text += f"Haz buscado por: {' '.join(argumentos)}"

    bot.send_message(chat_id=chat_id, text=text)
    return
