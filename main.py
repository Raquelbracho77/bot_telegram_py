
import logging

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

from conf import TOKEN

LOG_FORMAT = '%(asctime)s - %(levelname)s - %(message)s'
LOG_DATE = '%d/%m/%Y %H:%M:%S'
logging.basicConfig(level = logging.DEBUG, format = LOG_FORMAT, 
	datefmt = LOG_DATE)

log = logging.getLogger(__name__)

# ~ iniciar el bot
def start(update, context):
	# ~ el contexto da acceso al bot
	bot = context.bot
	# ~ chat_id llama al bot
	chat_id = update.effective_chat.id
	# ~ user, acá el usuario llama al bot
	user = update.message.from_user
	text = f'Hola {user.username}\n\nMi nombre es Qatana'
	# ~ bot envía mensaje
	bot.send_message(chat_id = chat_id, text = text)
	return
	
	
def ayuda(update, context):
	bot = context.bot
	chat_id = update.effective_chat.id
	text = """ Mis opciones son:
	
	/start - Muestra el mensaje inicial
	/ayuda - Lista de ayuda
	/buscar - buscar
	"""
	bot.send_message(chat_id = chat_id, text = text)
	return
	
	
def buscar(update, context):
	bot = context.bot
	chat_id = update.effective_chat.id
	user = update.message.from_user
	# ~ concatenar la lista
	text = f'Hola {user.username}\n\n'
	# ~ recibir lo que escribe el usuario despues de que utilice el comando
	# ~ esto es una lista
	argumentos = context.args
	# ~ con text += (porque es lista)
	text += f"Haz buscado por: {' '.join(argumentos)}"
	
	bot.send_message(chat_id = chat_id, text = text)
	return
	
	
	'''
CONVERSAR
	'''
def echo(update, context):
	
	if update.message.text in ("Hola", "agua", "tequila"):
		update.message.reply_text("ok")
	else:
		update.message.reply_text("No")
	return
	
def main():
	# ~ actualiza 
	updater = Updater(token = TOKEN, use_context = True)
	dp = updater.dispatcher
	# ~ commandHandler, es una funcion a la que pasamos los argumentos
	# ~ como texto, el nombre del comando, por ejemplo 'start' 
	dp.add_handler(CommandHandler('start', start))
	dp.add_handler(CommandHandler('ayuda', ayuda))
	dp.add_handler(CommandHandler('buscar', buscar))
	
	# esto es para responder o hacer eco del mensaje respecto a la funcion echo
	dp.add_handler(MessageHandler(Filters.text & ~Filters.command, echo))

	
	# ~ inicia
	updater.start_polling()
	# ~ acá queda 'escuchando' el bot siempre que este activo
	updater.idle()
	
	
	return
	
	

if __name__ == '__main__':
	main()
